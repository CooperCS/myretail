package com.myretail.config;

import static org.mockito.Mockito.mock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;

import com.myretail.external.ExternalAPIs;
import com.myretail.persistance.NitriteRepository;

@Configuration
public class TestConfig {
	
	@Bean
	public ExternalAPIs externalAPIs(){
		return new ExternalAPIs();
	}
	
	@Bean
	public NitriteRepository nitriteRepository(){
		return new NitriteRepository();
	}
	
	@Profile("!e1")
	@Bean(name="mockRest")
	public RestTemplate restTemplateMock(){
		return mock(RestTemplate.class);
	}
	
	@Profile("e1")
	@Bean
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}
}
