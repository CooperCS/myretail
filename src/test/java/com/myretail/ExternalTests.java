package com.myretail;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.myretail.config.TestConfig;
import com.myretail.constants.ExternalURLs;
import com.myretail.external.ExternalAPIs;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = TestConfig.class)
public class ExternalTests {
	
	@Autowired
	private RestTemplate mockRestTemplate;
	@Autowired
	private ExternalAPIs externalAPIs;
	private String url = ExternalURLs.PRODUCT_NAME_BASE_URL.replace("PRODUCTID", "123");
	
	@Test
	public void productNameHappy(){
		ResponseEntity<String> response = new ResponseEntity<String>(testName(), HttpStatus.ACCEPTED);
		when(mockRestTemplate.getForEntity(url, String.class)).thenReturn(response);
		String result = externalAPIs.getProductName(123);
		assertEquals("test name", result);
	}
	
	@Test
	public void productNameFail(){
		when(mockRestTemplate.getForEntity(url, String.class)).thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST));
		String result = externalAPIs.getProductName(123);
		assertEquals(null, result);
	}
	
	private String testName(){
		return "{\"product\": {\"item\":{\"product_description\":{\"title\":\"test name\"}}}}";
	}

}
