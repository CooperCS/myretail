package com.myretail;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.myretail.config.TestConfig;
import com.myretail.model.CurrentPrice;
import com.myretail.model.MyRetailResponse;
import com.myretail.persistance.NitriteRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = TestConfig.class)
public class NitriteRepoTests {
	private int existingId = 13860425;
	
	@Autowired
	NitriteRepository nitriteRepo;

	@Test
	public void getPricingTest(){
		CurrentPrice currentPrice = nitriteRepo.getPricing(existingId);
		assertNotNull(currentPrice);
		assertNotNull(currentPrice.getValue());
		assertNotNull(currentPrice.getCurrency_code());
	}
	
	@Test
	public void putPricingTest(){
		MyRetailResponse myRetail = new MyRetailResponse();
		myRetail.setId(existingId);
		myRetail.setCurrent_price(new CurrentPrice(3.99, "LUX"));
		nitriteRepo.putPricing(myRetail);
		CurrentPrice currentPrice = nitriteRepo.getPricing(existingId);
		assertEquals(3.99, currentPrice.getValue(), 0.05);
		assertEquals("LUX", currentPrice.getCurrency_code());
	}
	
}
