package com.myretail;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.myretail.config.TestConfig;
import com.myretail.external.ExternalAPIs;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = TestConfig.class)
public class GetRealProductNameTest {
	
	@Autowired
	private ExternalAPIs externalAPIs;
	@Autowired
	Environment env;
	int realProductId = 13860428;
	
	//this test will hit the real API.  This test won't run unless it is run with the 'e1' profile
	@Test
	public void getRealProductName(){
		if(Arrays.toString(env.getActiveProfiles()).contains("e1")){
			System.out.println("running real api test");
			String result = externalAPIs.getProductName(realProductId);
			assertTrue("result is null when an actual string name was expected", result != null);
		}
	}
}
