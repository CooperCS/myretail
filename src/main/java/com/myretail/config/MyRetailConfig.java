package com.myretail.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.myretail.external.ExternalAPIs;
import com.myretail.persistance.NitriteRepository;
import com.myretail.persistance.Repository;

@Component
public class MyRetailConfig {
	
	@Bean
	public ExternalAPIs externalAPIs(){
		return new ExternalAPIs();
	}
	
	@Bean
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}

	@Bean
	public Repository NitriteRepository(){
		return new NitriteRepository();
	}
}
