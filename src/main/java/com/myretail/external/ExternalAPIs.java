package com.myretail.external;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myretail.constants.ExternalURLs;

public class ExternalAPIs {
	
	@Autowired
	private RestTemplate restTemplate;
	
	public String getProductName(int id){
		String url = ExternalURLs.PRODUCT_NAME_BASE_URL.replace("PRODUCTID", String.valueOf(id));
		JsonNode root = null;
	    JsonNode title = null;
		try {
		    ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
		    ObjectMapper mapper = new ObjectMapper();
			root = mapper.readTree(response.getBody());
			title = root.path("product").path("item").path("product_description").path("title");
		} catch (HttpClientErrorException e) {
			//TODO add log
			e.printStackTrace();
		} catch (IOException e) {
			//TODO add log
			e.printStackTrace();
		}
	    
		return title == null ? null : title.asText();
	}
}
