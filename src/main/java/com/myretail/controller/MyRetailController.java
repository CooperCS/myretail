package com.myretail.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.myretail.constants.Responses;
import com.myretail.external.ExternalAPIs;
import com.myretail.model.MyRetailResponse;
import com.myretail.model.ResponseError;
import com.myretail.persistance.Repository;

@RestController
@RequestMapping("/v1/products")
public class MyRetailController {
	
	@Autowired
	private Repository repository;
	@Autowired
	private ExternalAPIs externalAPIs;
	
	//GET for fetching information of a product by product id
	@RequestMapping(
			value = "/{id}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getProductInfo(@PathVariable int id){
		MyRetailResponse response = new MyRetailResponse();
		response.setId(id);
		response.setName(externalAPIs.getProductName(id));
		response.setCurrent_price(repository.getPricing(id));
		
		return response.getName() == null ? 
			new ResponseEntity<ResponseError>(new ResponseError(Responses.GET_NAME_FAILURE), HttpStatus.BAD_REQUEST) :
			response.getCurrent_price() == null ? 
				new ResponseEntity<ResponseError>(new ResponseError(Responses.GET_PRICE_FAILURE), HttpStatus.BAD_REQUEST) :
				new ResponseEntity<MyRetailResponse>(response, HttpStatus.OK);
	}
	
	//PUT for updating pricing information of a product
	@RequestMapping(
			value = "/{id}",
			method = RequestMethod.PUT,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> setPricing(@PathVariable int id, @RequestBody MyRetailResponse pricing){
		pricing.setId(id);
		return repository.putPricing(pricing) ? 
				  new ResponseEntity<String>(Responses.PUT_SUCCESS, HttpStatus.OK) :
				  new ResponseEntity<String>(Responses.PUT_FAILURE, HttpStatus.BAD_REQUEST);
	}
}