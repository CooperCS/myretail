package com.myretail.constants;

public class Responses {
	
	public static final String PUT_SUCCESS = "Pricing update successful.";
	public static final String PUT_FAILURE = "Pricing update failed.";
	public static final String GET_NAME_FAILURE = "Couldn't retrieve product name.";
	public static final String GET_PRICE_FAILURE = "Couldn't find product price.";

}
