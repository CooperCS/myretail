package com.myretail.constants;

public class ExternalURLs {
	
	public static final String PRODUCT_NAME_BASE_URL = "https://redsky.target.com/v2/pdp/tcin/PRODUCTID?excludes=taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics,deep_red_labels,available_to_promise_network";

}
