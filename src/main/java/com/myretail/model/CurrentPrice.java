package com.myretail.model;

import java.io.Serializable;

public class CurrentPrice implements Serializable {
	private static final long serialVersionUID = -7143537496539880818L;
	private Double value;
	private String currency_code;
	
	public CurrentPrice(){}
	
	public CurrentPrice(double value, String currency_code){
		this.value = value;
		this.currency_code = currency_code;
	}
	
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public String getCurrency_code() {
		return currency_code;
	}
	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}
}
