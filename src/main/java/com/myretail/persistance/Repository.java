package com.myretail.persistance;

import com.myretail.model.CurrentPrice;
import com.myretail.model.MyRetailResponse;

public interface Repository {
	
	public CurrentPrice getPricing(int id);
	
	public boolean putPricing(MyRetailResponse data);
}
