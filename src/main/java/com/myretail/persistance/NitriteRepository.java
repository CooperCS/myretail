package com.myretail.persistance;

import org.dizitart.no2.Cursor;
import org.dizitart.no2.Document;
import org.dizitart.no2.Nitrite;
import org.dizitart.no2.NitriteCollection;
import org.dizitart.no2.filters.Filters;

import com.myretail.model.CurrentPrice;
import com.myretail.model.MyRetailResponse;

public class NitriteRepository implements Repository {
	
	private Nitrite db;
	private NitriteCollection pricing;
	
	public NitriteRepository(){
		initialize();
	}

	@Override
	public CurrentPrice getPricing(int id) {
		Document doc = findDocument(id);
		return doc == null ? null : (CurrentPrice) doc.get("current_price");
	}

	@Override
	public boolean putPricing(MyRetailResponse data) {
		boolean result = false;
		if(data == null || data.getCurrent_price() == null 
						|| data.getCurrent_price().getValue() == null
						|| data.getCurrent_price().getCurrency_code() == null){
			return result;
		}
		Document doc = findDocument(data.getId());
		if(doc != null){
			doc.put("current_price", data.getCurrent_price());
			pricing.update(doc);
			result = true;
		} else {
			//TODO if we want to create a new CurrentPrice object and store it if we don't find one, uncomment below line
//			createDoc(String.valueOf(data.getId()), data.getCurrent_price());
		}
		return result;
	}
	
	private void initialize(){
		//start the in-memory nosql database
		db = Nitrite.builder().openOrCreate();
		pricing = db.getCollection("pricing");
		
		initialPopulation();
	}
	
	private void initialPopulation(){
		createDoc("13860429", new CurrentPrice(22.50, "CAD"));
		createDoc("13860428", new CurrentPrice(12.95, "USD"));
		createDoc("13860425", new CurrentPrice(19.99, "USD"));
		createDoc("13860424", new CurrentPrice(11.11, "AUS"));
		createDoc("13860421", new CurrentPrice(39.95, "GBT"));
		createDoc("13860420", new CurrentPrice(1.50, "JPN"));
		createDoc("13860419", new CurrentPrice(22.49, "MEX"));
	}
	
	private void createDoc(String productId, CurrentPrice current_price){
		Document doc = new Document().put("productId", productId)
				 .put("current_price", current_price);
		pricing.insert(doc);
	}
	
	private Document findDocument(int id){
		Document doc = null;
		Cursor cursor = pricing.find(Filters.eq("productId", String.valueOf(id)));
		
		for(Document document : cursor){
			doc = document;
			break;
		}
		return doc;
	}
}