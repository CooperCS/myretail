## myRetail

*made by Michael Cooper, 10.08.2018*

---

myRetail is a proof-of-concept REST application allowing users to get detailed information of a product
by making a GET request with a given product id.  The application also allows users to update pricing
information for a given id with a PUT request.

## How To Use

The myRetail repository can be found here:

https://bitbucket.org/CooperCS/myretail/src/master/

To use myRetail, you can clone the git project with the following command:

git clone https://CooperCS@bitbucket.org/CooperCS/myretail.git

Once cloned, you can run the application through an IDE like Eclipse, or build the project with Maven and run
the application jar with the following command:

java -jar path/to/jar/myretail-0.0.1-SNAPSHOT.jar

## GET localhost:8080/v1/products/{id}

Hitting this GET api with a given product id will give back a payload with product name and price information.

Example:

{
    "id": 13860428,
    "name": "The Big Lebowski (Blu-ray)",
    "current_price": {
        "value": 12.95,
        "currency_code": "USD"
    }
}

## PUT localhost:8080/v1/products/{id}

Hitting this PUT api can update existing product pricing information for the given product id.

Example request payload:

{
    "current_price": {
        "value": 19.99,
        "currency_code": "USD"
    }
}

The response will let you know if the update was successful, or a failure.

## Promoting To Production

myRetail application uses Spring Boot to implement its REST APIs, as well as to run on an embedded 
tomcat web server.  This application could be deployed on a production server within a docker.
Regarding scalability, multiple instances of the application could be deployed across multiple servers,
with a main API url delegating work by forwarding the requests to all running instances of the application.

Spring REST APIs are multi-threaded, which will work well for the GET requests coming in.  We would
have to be careful with the PUT request, as multiple threads could be trying to update the same
data.  That would need to be addressed either within the data storage solution, or within the application.

myRetail doesn't have any security.  A security solution like OAuth could be easily implemented.

There is basic validation with myRetail, but more robust exception-handling would be ideal for a 
production application.

myRetail has a handful of unit tests, but more extensive testing would be required for quality assurance.

myRetail doesn't have any logging, so some logging would be would need to be implemented throughout the application.

## Data Storage

myRetail implements an in-memory noSQL data storage solution called Nitrite:

https://www.dizitart.org/nitrite-database.html

Nitrite was chosen to make this proof-of-concept as portable as possible.  With an in-memory solution,
the application can be entirely contained in and run from a single jar.  For production, a more robust
noSQL solution might be preferred.

## Usable Product IDs

The way myRetail is set up, only existing pricing data can be updated.  If there isn't already pricing data for a
given product id, the PUT request will fail.  Here is the list of product ids that have preloaded pricing data
which can be updated:

  `13860429`
  `13860428`
  `13860425`
  `13860424`
  `13860421`
  `13860420`
  `13860419`